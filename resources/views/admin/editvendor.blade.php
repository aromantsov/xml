@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<form action="{{ route('updatevendor', $vendor->id) }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT"> 
            <input type="hidden" name="id" value="{{ $vendor->id }}"> 
			{{ csrf_field() }}
			<div class="panel-body">
                <label for="name">Наименование поставщика</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Наименование поставщика" value="{{ $vendor->name }}" required>
            </div>
            <div class="panel-body">
                <label for="name">Ссылка на файл</label>
                <input type="text" class="form-control" name="file_link" id="file_link" placeholder="" value="{{ $vendor->file_link or 'http://' }}">
            </div>
            <p>или</p>
            <div class="panel-body">
                <label for="file">Файл</label>
                <input type="file" class="form-control" name="file" id="file" placeholder="Загрузите файл" value="{{ $vendor->file }}">
            </div>
            <div class="panel-body">
                <label for="language">Язык предоставления информации</label>
                <select name="language" id="language" class="form-control">
                	<option value="1" @if($vendor->language == 1) selected @endif>Русский</option>
                	<option value="2" @if($vendor->language == 2) selected @endif>Украинский</option>
                </select>
            </div>
            <div class="panel-body">
                <label for="changeprice">Ценовой коэффициент (все цены будут умножаться на это число, по умолчанию 1)</label>
                <input type="text" class="form-control" name="changeprice" id="changeprice" placeholder="" value="{{ $vendor->changeprice or 1 }}" required>
            </div>
            <div class="panel-body">
                <label for="products">Путь до тегов категорий (полная вложенность, теги разделяются точкой. Например yml_catalog.shop.categories)</label>
                <input type="text" class="form-control" name="categories_sector" id="categories_sector" placeholder="Путь до тегов категорий" value="{{ $vendor->categories_sector }}">
            </div>
            <div class="panel-body">
                <label for="products">Тег категории (тег, в котором заключены данные категории)</label>
                <input type="text" class="form-control" name="category_tag" id="category_tag" placeholder="Тег категории" value="{{ $vendor->category_tag }}">
            </div>
             <div class="panel-body">
                <label for="products">Путь до тегов товаров (полная вложенность, теги разделяются точкой. Например yml_catalog.shop.offers)</label>
                <input type="text" class="form-control" name="products" id="products" placeholder="Путь до тегов товаров" value="{{ $vendor->products }}" required>
            </div>
            <div class="panel-body">
                <label for="products">Тег товарa (тег, в котором заключены данные товарной единицы)</label>
                <input type="text" class="form-control" name="offer_tag" id="offer_tag" placeholder="Тег товара" value="{{ $vendor->offer_tag
                 }}" required>
            </div>
            <h4>Теги, определяющие параметры товара</h4>
            <p>(через точку указывается атрибут, например param.name, аттрибуты заглавного тега товара начинаются с @, например @id)</p>
            <div class="panel-body">
                <label for="tag_id">ID товара</label>
                <input type="text" class="form-control" name="tag_id" id="tag_id" placeholder="ID товара" value="{{ $vendor->tag_id }}" required>
            </div>
            <div class="panel-body">
                <label for="tag_available">Доступность</label>
                <input type="text" class="form-control" name="tag_available" id="tag_available" placeholder="Доступность" value="{{ $vendor->tag_available or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_group">Группа товаров по опциям</label>
                <input type="text" class="form-control" name="tag_group" id="tag_group" placeholder="Группа товаров по опциям" value="{{ $vendor->tag_group or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_url">url</label>
                <input type="text" class="form-control" name="tag_url" id="tag_url" placeholder="url" value="{{ $vendor->tag_url or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_price">Цена товара</label>
                <input type="text" class="form-control" name="tag_price" id="tag_price" placeholder="Цена товара" value="{{ $vendor->tag_price or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_oldprice">Цена без скидки</label>
                <input type="text" class="form-control" name="tag_oldprice" id="tag_oldprice" placeholder="Цена без скидки" value="{{ $vendor->tag_oldprice or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_quantity">Количество</label>
                <input type="text" class="form-control" name="tag_quantity" id="tag_quantity" placeholder="Количество" value="{{ $vendor->tag_quantity or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_currency">Валюта, указанная в цене товара</label>
                <input type="text" class="form-control" name="tag_currency" id="tag_currency" placeholder="Валюта" value="{{ $vendor->tag_currency or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_category">ID категории товара</label>
                <input type="text" class="form-control" name="tag_category" id="tag_category" placeholder="ID категории товара" value="{{ $vendor->tag_category or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_picture">Изображения товара (первое изображение является основным)</label>
                <input type="text" class="form-control" name="tag_picture" id="tag_picture" placeholder="Изображения товара" value="{{ $vendor->tag_picture or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_name">Название товара</label>
                <input type="text" class="form-control" name="tag_name" id="tag_name" placeholder="Название товара" value="{{ $vendor->tag_name or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_manufacturer">Производитель</label>
                <input type="text" class="form-control" name="tag_manufacturer" id="tag_manufacturer" placeholder="Производитель" value="{{ $vendor->tag_manufacturer or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_barcode">Заводской артикул товара</label>
                <input type="text" class="form-control" name="tag_barcode" id="tag_barcode" placeholder="Заводской артикул" value="{{ $vendor->tag_barcode or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_description">Описание товара</label>
                <input type="text" class="form-control" name="tag_description" id="tag_description" placeholder="Описание товара" value="{{ $vendor->tag_description or '' }}">
            </div>
            <div class="panel-body">
                <label for="tag_param">Характеристики товара</label>
                <input type="text" class="form-control" name="tag_param" id="tag_param" placeholder="Характеристики товара" value="{{ $vendor->tag_param or '' }}">
            </div>
            Выберите категории (в списке представлены категории на сайте diceramica, укажите id категорий файла (через запятую), которые нужно записать в данную категорию. Не выбранные категории записываются в категорию с таким же названием, если ее нет - создается новая)
            @foreach($categories as $category)
            <div class="panel-body">
                <label for="category-{{ $category->id }}">{{ $category->name }}</label>
                <input type="text" class="form-control" name="category[{{ $category->name }}]" id="category-{{ $category->id }}" placeholder="" value="{{ $vendor->categories[$category->name] or '' }}">
            </div>
            @endforeach
            <div class="panel-body">
                <button type="submit" name="some_name" class="btn btn-primary" value="save">Сохранить настройки</button>
            </div>
		</form>
	</div>
</div>
@endsection