@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel-body">
                <a href="{{ route('addvendor') }}">Добавить поставщика</a>
            </div>

            <table class="table table-striped">
                <thead>
                    <th>Поставщик</th>
                    <th class="text-right"></th>
                </thead>
                <tbody>
                    @forelse($vendors as $vendor)
                    <tr>
                        <td>{{ $vendor->name }}</td>
                        <td class="text-right">
                            <form action="{{ route('removevendor', $vendor->id) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
                                <input type="hidden" name="_method" value="DELETE"> 
                                {{ csrf_field() }}
                                <a href="{{ route('upload', $vendor->id) }}" class="btn btn-default"><i class="fa fa-upload"></i></a>
                                <a href="{{ route('editvendor', $vendor->id) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                <button type="submit" class="btn"><i class="fa fa-trash-alt"></i></button>
                            </form></td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3" class="text-center"><h2>Нет поставщиков</h2></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection