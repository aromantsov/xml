@php
use App\Http\Controllers\Admin\CategoryController;
@endphp
<tr>
<td>{{ $child_category->name }}</td>
<td>{{ $child_category->parent }}</td>
<td class="text-right">
                            <form action="{{ route('deletecategory', $child_category->id) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
                                <input type="hidden" name="_method" value="DELETE"> 
                                {{ csrf_field() }}
                                <a href="{{ route('editcategory', $child_category->id) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                <button type="submit" class="btn"><i class="fa fa-trash-alt"></i></button>
                            </form></td>
@if ($child_category->children)
       
        @foreach ($child_category->children as $childCategory)
            @php
            $childCategory->parent = CategoryController::getParent($childCategory->parent_id);
            @endphp
            @include('admin.child_category', ['child_category' => $childCategory]) 
        @endforeach

@endif