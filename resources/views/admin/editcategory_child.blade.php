@php
use App\Http\Controllers\Admin\CategoryController;
$child_category->name = $child_category->parent . ' - ' . $child_category->name;
@endphp
<option value="{{ $child_category->id }}" @if($category->parent_id == $child_category->id) selected @endif>{{ $child_category->name }}</option>
@if ($child_category->children)
        
        @foreach ($child_category->children as $childCategory)
            @php
            $childCategory->parent = CategoryController::getParent($childCategory->parent_id);
            @endphp
            @include('admin.addcategory_child', ['child_category' => $childCategory]) 
        @endforeach

@endif