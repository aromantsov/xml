@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
<form action="{{ route('export') }}" method="get">
    <div class="panel-body">
        <label for="start">Начало</label>
        <input type="text" class="form-control" name="start" id="start" placeholder="Начало" value="" required>
    </div>
    <div class="panel-body">
        <label for="limit">Конец</label>
        <input type="text" class="form-control" name="end" id="end" placeholder="Начало" value="" required>
    </div>
    <div class="panel-body">
        <button type="submit" name="submit" class="btn btn-primary" value="export">Получить xml</button>
    </div>
</form>
</div></div>
@endsection