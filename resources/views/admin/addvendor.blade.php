@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<form action="{{ route('savevendor') }}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="panel-body">
                <label for="name">Наименование поставщика</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Наименование поставщика" value="" required>
            </div>
            <div class="panel-body">
                <label for="name">Ссылка на файл</label>
                <input type="text" class="form-control" name="file_link" id="file_link" placeholder="" value="http://">
            </div>
            <p>или</p>
            <div class="panel-body">
                <label for="file">Файл</label>
                <input type="file" class="form-control" name="file" id="file" placeholder="Загрузите файл" value="">
            </div>
            <div class="panel-body">
                <label for="language">Язык предоставления информации</label>
                <select name="language" id="language" class="form-control">
                	<option value="1">Русский</option>
                	<option value="2">Украинский</option>
                </select>
            </div>
            <div class="panel-body">
                <label for="changeprice">Ценовой коэффициент (все цены будут умножаться на это число, по умолчанию 1)</label>
                <input type="text" class="form-control" name="changeprice" id="changeprice" placeholder="" value="1" required>
            </div>
            <div class="panel-body">
                <label for="products">Путь до тегов категорий (полная вложенность, теги разделяются точкой. Например yml_catalog.shop.categories)</label>
                <input type="text" class="form-control" name="categories_sector" id="categories_sector" placeholder="Путь до тегов категорий" value="">
            </div>
            <div class="panel-body">
                <label for="products">Тег категории (тег, в котором заключены данные категории)</label>
                <input type="text" class="form-control" name="category_tag" id="category_tag" placeholder="Тег категории" value="">
            </div>
            <div class="panel-body">
                <label for="products">Путь до тегов товаров (полная вложенность, теги разделяются точкой. Например yml_catalog.shop.offers)</label>
                <input type="text" class="form-control" name="products" id="products" placeholder="Путь до тегов товаров" value="" required>
            </div>
            <div class="panel-body">
                <label for="products">Тег товарa (тег, в котором заключены данные товарной единицы)</label>
                <input type="text" class="form-control" name="offer_tag" id="offer_tag" placeholder="Тег товара" value="" required>
            </div>
            <h4>Теги, определяющие параметры товара</h4>
            <p>(через точку указывается атрибут, например param.name, аттрибуты заглавного тега товара начинаются с @, например @id)</p>
            <div class="panel-body">
                <label for="tag_id">ID товара</label>
                <input type="text" class="form-control" name="tag_id" id="tag_id" placeholder="ID товара" value="" required>
            </div>
            <div class="panel-body">
                <label for="tag_available">Доступность</label>
                <input type="text" class="form-control" name="tag_available" id="tag_available" placeholder="Доступность" value="">
            </div>
            <div class="panel-body">
                <label for="tag_group">Группа товаров по опциям</label>
                <input type="text" class="form-control" name="tag_group" id="tag_group" placeholder="Группа товаров по опциям" value="">
            </div>
            <div class="panel-body">
                <label for="tag_url">url</label>
                <input type="text" class="form-control" name="tag_url" id="tag_url" placeholder="url" value="">
            </div>
            <div class="panel-body">
                <label for="tag_price">Цена товара</label>
                <input type="text" class="form-control" name="tag_price" id="tag_price" placeholder="Цена товара" value="">
            </div>
            <div class="panel-body">
                <label for="tag_oldprice">Цена без скидки</label>
                <input type="text" class="form-control" name="tag_oldprice" id="tag_oldprice" placeholder="Цена без скидки" value="">
            </div>
            <div class="panel-body">
                <label for="tag_quantity">Количество</label>
                <input type="text" class="form-control" name="tag_quantity" id="tag_quantity" placeholder="Количество" value="">
            </div>
            <div class="panel-body">
                <label for="tag_currency">Валюта, указанная в цене товара</label>
                <input type="text" class="form-control" name="tag_currency" id="tag_currency" placeholder="Валюта" value="">
            </div>
            <div class="panel-body">
                <label for="tag_category">ID категории товара</label>
                <input type="text" class="form-control" name="tag_category" id="tag_category" placeholder="Валюта" value="">
            </div>
            <div class="panel-body">
                <label for="tag_picture">Изображения товара (первое изображение является основным)</label>
                <input type="text" class="form-control" name="tag_picture" id="tag_picture" placeholder="Изображения товара" value="">
            </div>
            <div class="panel-body">
                <label for="tag_name">Название товара</label>
                <input type="text" class="form-control" name="tag_name" id="tag_name" placeholder="Название товара" value="">
            </div>
            <div class="panel-body">
                <label for="tag_manufacturer">Производитель</label>
                <input type="text" class="form-control" name="tag_manufacturer" id="tag_manufacturer" placeholder="Производитель" value="">
            </div>
            <div class="panel-body">
                <label for="tag_barcode">Заводской артикул товара</label>
                <input type="text" class="form-control" name="tag_barcode" id="tag_barcode" placeholder="Заводской артикул" value="">
            </div>
            <div class="panel-body">
                <label for="tag_description">Описание товара</label>
                <input type="text" class="form-control" name="tag_description" id="tag_description" placeholder="Описание товара" value="">
            </div>
            <div class="panel-body">
                <label for="tag_param">Характеристики товара</label>
                <input type="text" class="form-control" name="tag_param" id="tag_param" placeholder="Характеристики товара" value="">
            </div>
            
            Выберите категории (в списке представлены категории на сайте diceramica, укажите id категорий файла (через запятую), которые нужно записать в данную категорию. Не выбранные категории записываются в категорию с таким же названием, если ее нет - создается новая)
            @foreach($categories as $category)
            <div class="panel-body">
                <label for="category-{{ $category->id }}">{{ $category->name }}</label>
                <input type="text" class="form-control" name="category[{{ $category->name }}]" id="category-{{ $category->id }}" placeholder="" value="">
            </div>
            @endforeach
            <div class="panel-body">
                <button type="submit" name="some_name" class="btn btn-primary" value="save">Сохранить настройки</button>
            </div>
		</form>
	</div>
</div>
@endsection