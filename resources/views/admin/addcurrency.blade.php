@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<form action="{{ route('savecurrency') }}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="panel-body">
                <label for="name">Наименование валюты</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Наименование валюты" value="" required>
            </div>
            <div class="panel-body">
                <label for="code">Код валюты</label>
                <input type="text" class="form-control" name="code" id="code" placeholder="Код валюты" value="" required>
            </div>
            <div class="panel-body">
                <label for="rate">Курс валюты</label>
                <input type="text" class="form-control" name="rate" id="rate" placeholder="Курс валюты" value="" required>
            </div>
            <div class="panel-body">
                <button type="submit" name="some_name" class="btn btn-primary" value="save">Сохранить настройки</button>
            </div>
		</form>
	</div>
</div>
@endsection