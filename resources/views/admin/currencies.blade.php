@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel-body">
                <a href="{{ route('addcurrency') }}">Добавить валюту</a>
            </div>

            <table class="table table-striped">
                <thead>
                    <th>Валюта</th>
                    <th>Код валюты</th>
                    <th>Курс валюты</th>
                    <th class="text-right"></th>
                </thead>
                <tbody>
                    @forelse($currencies as $currency)
                    <tr>
                        <td>{{ $currency->name }}</td>
                        <td>{{ $currency->code }}</td>
                        <td>{{ $currency->rate }}</td>
                        <td class="text-right">
                            <form action="{{ route('deletecurrency', $currency->id) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
                                <input type="hidden" name="_method" value="DELETE"> 
                                {{ csrf_field() }}
                                <a href="{{ route('editcurrency', $currency->id) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                <button type="submit" class="btn"><i class="fa fa-trash-alt"></i></button>
                            </form></td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3" class="text-center"><h2>Нет валют</h2></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection