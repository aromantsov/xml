@php
use App\Http\Controllers\Admin\CategoryController;
@endphp
@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<form action="{{ route('savecategory') }}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="panel-body">
                <label for="name">Наименование категории</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Наименование категории" value="" required>
            </div>
            <div class="panel-body">
                <label for="parent">Родительская категория</label>
                <select name="parent_id" id="parent" class="form-control">
                    <option value="0">Нет родительской категории</option>
                    @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @foreach ($category->children as $childCategory)
                        @php
                        $childCategory->parent = CategoryController::getParent($childCategory->parent_id);
                        @endphp
                        @include('admin.addcategory_child', ['child_category' => $childCategory])
                    @endforeach
                    @endforeach
                </select>
            </div>
            <div class="panel-body">
                <button type="submit" name="some_name" class="btn btn-primary" value="save">Сохранить настройки</button>
            </div>
		</form>
	</div>
</div>
@endsection