@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
        <a href="{{ route('vendors') }}">Поставщики</a>
    </div>
    <div class="row">
        <a href="{{ route('addvendor') }}">Добавить поставщика</a>
    </div>
    <br/>
    <div class="row">
        <a href="{{ route('currencies') }}">Валюты</a>
    </div>
    <div class="row">
        <a href="{{ route('addcurrency') }}">Добавить валюту</a>
    </div>
    <br/>
    <div class="row">
        <a href="{{ route('categories') }}">Категории</a>
    </div>
    <div class="row">
        <a href="{{ route('addcategory') }}">Добавить категорию</a>
    </div>
    <br/>
    <div class="row">
        <a href="{{ route('xml') }}" target="_blank">Получить результирующий xml-файл</a> 
    </div>
</div>
@endsection
