@php
use App\Http\Controllers\Admin\CategoryController;
@endphp
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel-body">
                <a href="{{ route('addcategory') }}">Добавить категорию</a>
            </div>

            <table class="table table-striped">
                <thead>
                    <th>Категория</th>
                    <th>Родительская категория</th>
                    <th class="text-right"></th>
                </thead>
                <tbody>
                    @forelse($categories as $category)
                    <tr>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->parent }}</td>
                        <td class="text-right">
                            <form action="{{ route('deletecategory', $category->id) }}" onsubmit="if(confirm('Delete?')){return true}else{return false}" method="post">
                                <input type="hidden" name="_method" value="DELETE">
                                {{ csrf_field() }}
                                <a href="{{ route('editcategory', $category->id) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                <button type="submit" class="btn"><i class="fa fa-trash-alt"></i></button>
                            </form></td>
                    </tr>
                    @foreach ($category->children as $childCategory)
                        @php
                        $childCategory->parent = CategoryController::getParent($childCategory->parent_id);
                        @endphp
                        @include('admin.child_category', ['child_category' => $childCategory])
                    @endforeach
                    @empty
                    <tr>
                        <td colspan="3" class="text-center"><h2>Нет категорий</h2></td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection