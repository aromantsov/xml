@php
use App\Http\Controllers\Admin\CategoryController;
@endphp
@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<form action="{{ route('updatecategory', $category->id) }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
			{{ csrf_field() }}
			<div class="panel-body">
                <label for="name">Наименование категории</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Наименование категории" value="{{ $category->name }}" required>
            </div>
            <div class="panel-body">
                <label for="parent">Родительская категория</label>
                <select name="parent_id" id="parent" class="form-control">
                    <option value="0">Нет родительской категории</option>
                    @foreach($all_categories as $one_category)
                    <option value="{{ $one_category->id }}" @if($category->parent_id == $one_category->id) selected @endif>{{ $one_category->name }}</option>
                    @foreach ($one_category->children as $childCategory)
                        @php
                        $childCategory->parent = CategoryController::getParent($childCategory->parent_id);
                        @endphp
                        @include('admin.editcategory_child', ['child_category' => $childCategory, 'category' => $category])
                    @endforeach
                    @endforeach
                </select>
            </div>
            <div class="panel-body">
                <button type="submit" name="some_name" class="btn btn-primary" value="save">Сохранить настройки</button>
            </div>
		</form>
	</div>
</div>
@endsection