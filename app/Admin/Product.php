<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    public static function addProduct($data){
        $category_ids = [];
        foreach($data['category'] as $cat){
            $category = DB::table('categories')->where('name', '=', $cat)->first();
            $category_ids[] = $category->id;
        }
    	//$category = DB::table('categories')->where('name', '=', $data['category'])->first();
        $category_ids = implode(', ', $category_ids);

    	$fields = [
    		'vendor_id' => $data['vendor_id'],
    		'src_id' => $data['src_id'],
    		'available' => $data['available'],
    		'group_id' => $data['group_id'],
    		'url' => $data['url'],
    		'price' => $data['price'],
    		'oldprice' => $data['oldprice'],
    		'quantity' => $data['quantity'] ? $data['quantity'] : 0,
    		'picture' => $data['picture'],
    		'name' => $data['name'],
    		//'name_ua' => $data['name_ua'],
    		'manufacturer' => $data['manufacturer'],
    		'barcode' => $data['barcode'],
    		'description' => $data['description'],
    		//'description_ua' => $data['description_ua'],
    		'params' => $data['params'],
    		//'params_ua' => $data['params_ua'],
    		'category' => $category_ids ? $category_ids : '',
    		'created_at' => date('Y-m-d h:i:s'),
    		'updated_at' => date('Y-m-d h:i:s')
    	];
    	DB::table('products')->insert($fields);
    }

    public static function getProduct($data)
    {
        $product = DB::table('products')->where('name', '=', $data['name'])->where('vendor_id', '=', $data['vendor_id'])->first();
        return $product;
    }

    public static function updateProduct($data)
    {
        $fields = [
            'price' => $data['price'],
            'oldprice' => $data['oldprice'],
            'quantity' => $data['quantity'] ? $data['quantity'] : 0,
            'updated_at' => date('Y-m-d h:i:s')
        ];
        DB::table('products')->where('name', '=', $data['name'])->where('vendor_id', '=', $data['vendor_id'])->update($fields);
    }

    public static function getProducts($start, $end)
    {
        return DB::table('products')->whereBetween('id', [$start, $end])->get();
    }
}