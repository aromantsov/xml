<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Currency extends Model
{
    protected $fillable = ['name', 'code', 'rate'];

    public static function updateCurrency($currency, $data)
    {
    	$fields = [
            'name' => $data->name,
            'code' => $data->code,
            'rate' => $data->rate,
            'updated_at' => date('Y-m-d h:i:s')
    	];

    	DB::table('currencies')->where('id', '=', $currency)->update($fields);
    }

    public static function getCurrencyByCode($code)
    {
    	return DB::table('currencies')->where('code', '=', $code)->first();
    }
}