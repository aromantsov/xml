<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
	protected $fillable = ['name', 'parent_id'];

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id')->with('children');
    }

    public static function getParent($parent_id)
    {
    	$parent = DB::table('categories')->select('name')->where('id', '=', $parent_id)->first();
    	return $parent->name;
    }

    public static function updateCategory($category, $data)
    {
        $fields = [
            'name' => $data->name,
            'parent_id' => $data->parent_id,
            'updated_at' => date('Y-m-d h:i:s')
        ];

        DB::table('categories')->where('id', '=', $category)->update($fields);
    }
}
