<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Vendor extends Model
{
    protected $fillable = ['name', 'file_link', 'file', 'language', 'changeprice', 'products', 'offer_tag', 'categories_sector', 'category_tag', 'tag_id', 'tag_available', 'tag_group', 'tag_url', 'tag_price', 'tag_oldprice', 'tag_quantity', 'tag_currency', 'tag_category', 'tag_picture', 'tag_name', 'tag_manufacturer', 'tag_barcode', 'tag_description', 'tag_param', 'categories'];

    public static function updateVendorWithoutFile($vendor, $data, $categories)
    {
    	DB::table('vendors')->where('id', '=', $vendor)->update(['name' => $data->name, 'file_link' => $data->file_link, 'language' => $data->language, 'changeprice' => $data->changeprice, 'categories_sector' => $data->categories_sector, 'category_tag' => $data->category_tag, 'products' => $data->products, 'offer_tag' => $data->offer_tag, 'tag_id' => $data->tag_id, 'tag_available' => $data->tag_available, 'tag_group' => $data->tag_group, 'tag_url' => $data->tag_url, 'tag_price' => $data->tag_price, 'tag_oldprice' => $data->tag_oldprice, 'tag_quantity' => $data->tag_quantity, 'tag_currency' => $data->tag_currency, 'tag_category' => $data->tag_category, 'tag_picture' => $data->tag_picture, 'tag_name' => $data->tag_name, 'tag_manufacturer' => $data->tag_manufacturer, 'tag_barcode' => $data->tag_barcode, 'tag_description' => $data->tag_description, 'tag_param' => $data->tag_param, 'categories' => $categories, 'updated_at' => date('Y-m-d h:i:s')]);
    }

    public static function updateVendorWithFile($vendor, $data, $file, $categories)
    {
    	DB::table('vendors')->where('id', '=', $vendor)->update(['name' => $data->name, 'file_link' => $data->file_link, 'file' => $file, 'language' => $data->language, 'changeprice' => $data->changeprice,  'categories_sector' => $data->categories_sector, 'category_tag' => $data->category_tag, 'products' => $data->products, 'offer_tag' => $data->offer_tag, 'tag_id' => $data->tag_id, 'tag_available' => $data->tag_available, 'tag_group' => $data->tag_group, 'tag_url' => $data->tag_url, 'tag_price' => $data->tag_price, 'tag_oldprice' => $data->tag_oldprice, 'tag_quantity' => $data->tag_quantity, 'tag_currency' => $data->tag_currency, 'tag_category' => $data->tag_category, 'tag_picture' => $data->tag_picture, 'tag_name' => $data->tag_name, 'tag_manufacturer' => $data->tag_manufacturer, 'tag_barcode' => $data->tag_barcode, 'tag_description' => $data->tag_description, 'tag_param' => $data->tag_param, 'categories' => $categories, 'updated_at' => date('Y-m-d h:i:s')]);
    }
}
