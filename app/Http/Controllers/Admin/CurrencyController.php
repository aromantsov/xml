<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Currency;
use Admin;

class CurrencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
    	$currencies = Currency::all();
        return view('admin.currencies', ['currencies' => $currencies]);
    }

    public function add()
    {
    	return view('admin.addcurrency');
    }

    public function upload(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required|string|unique:currencies',
            'code' => 'required|string',
            'rate' => 'required'
    	]);

    	Currency::create([
    		'name' => $request['name'],
    		'code' => $request['code'],
    		'rate' => $request['rate']
    	]);
        return redirect()->route('currencies');
    }

    public function destroy($currency)
    {
        Currency::destroy($currency);
        return redirect()->route('currencies');
    }

    public function edit($currency_id)
    {
        $currency = Currency::find($currency_id);
        return view('admin.editcurrency', ['currency' => $currency]);
    }

    public function update(Request $request, $currency)
    {
        $this->validate($request, [
            'name' => ['required', 'string', \Illuminate\Validation\Rule::unique('currencies')->ignore($currency)],
            'code' => 'required|string',
            'rate' => 'required'
        ]);

        Currency::updateCurrency($currency, $request);
        return redirect()->route('currencies');
    }
}