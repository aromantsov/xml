<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Vendor;
use App\Admin\Category;

class VendorController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $vendors = Vendor::all();
        return view('admin.vendors', ['vendors' => $vendors]);
    }

    public function add()
    {
        $categories = Category::all();
        return view('admin.addvendor', ['categories' => $categories]);
    }

    public function upload(Request $request)
    {
        
        $this->validate($request, [
    		'name' => 'required|string|unique:vendors',
            'products' => 'required|string',
            'offer_tag' => 'required|string',
            'tag_id' => 'required' 
    	]);

        if($request->file()){
            foreach ($request->file() as $f) {
                $new_name = time().'_'.$f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . 'files', $new_name);
                $new_file = $_SERVER['DOCUMENT_ROOT'] . 'files/' . $new_name;
            }
        }else{
            $new_file = '';
        }

        $categories = json_encode($request['category'], JSON_UNESCAPED_UNICODE);

        $vendor = Vendor::create([
            'name' => $request['name'],
            'file_link' => $request['file_link'],
            'file' => $new_file, 
            'language' => $request['language'],
            'changeprice' => $request['changeprice'],
            'categories_sector' => $request['categories_sector'], 
            'category_tag' => $request['category_tag'],
            'products' => $request['products'], 
            'offer_tag' => $request['offer_tag'],
            'tag_id' => $request['tag_id'], 
            'tag_available' => $request['tag_available'], 
            'tag_group' => $request['tag_group'],
            'tag_url' => $request['tag_url'],
            'tag_price' => $request['tag_price'],
            'tag_oldprice' => $request['tag_oldprice'],
            'tag_quantity' => $request['tag_quantity'],
            'tag_currency' => $request['tag_currency'],
            'tag_category' => $request['tag_category'],
            'tag_picture' => $request['tag_picture'],
            'tag_name' => $request['tag_name'],
            'tag_manufacturer' => $request['tag_manufacturer'],
            'tag_barcode' => $request['tag_barcode'],
            'tag_description' => $request['tag_description'],
            'tag_param' => $request['tag_param'],
            'categories' => $categories
        ]);
        return redirect()->route('vendors');

    }

    public function destroy($vendor)
    {
        $vendor_char = Vendor::find($vendor);
        if($vendor_char->file){
            unlink($vendor_char->file);
        }
        Vendor::destroy($vendor);
        return redirect()->route('vendors');
    }

    public function edit($vendor)
    {
        $vendor_data = Vendor::find($vendor);
        $categories = Category::all();
        $vendor_data->categories = json_decode($vendor_data->categories, true);
        return view('admin.editvendor', ['vendor' => $vendor_data, 'categories' => $categories]);
    }

    public function update(Request $request, $vendor)
    {
        $this->validate($request, [
            'name' => ['required', 'string', \Illuminate\Validation\Rule::unique('vendors')->ignore($vendor)],
            'products' => 'required|string',
            'offer_tag' => 'required|string',
            'tag_id' => 'required' 
        ]);

        if($request->file()){
            $vendor_del = Vendor::find($request->id);
            unlink($_SERVER['DOCUMENT_ROOT'] . 'files/' . $vendor_del->file);

            foreach ($request->file() as $f) {
                $new_name = time().'_'.$f->getClientOriginalName();
                $f->move($_SERVER['DOCUMENT_ROOT'] . 'files', $new_name);
                $new_file = $_SERVER['DOCUMENT_ROOT'] . 'files/' . $new_name;
                $categories = json_encode($request['category'], JSON_UNESCAPED_UNICODE);
                Vendor::updateVendorWithFile($vendor, $request, $new_name, $categories);
            }
        }else{
            $categories = json_encode($request['category'], JSON_UNESCAPED_UNICODE);
            Vendor::updateVendorWithoutFile($vendor, $request, $categories);
        }
        $vendor_data = Vendor::find($vendor);
        $categories = Category::all();
        return view('admin.editvendor', ['vendor' => $vendor_data, 'categories' => $categories, 'success' => 'success']);
    }
}
