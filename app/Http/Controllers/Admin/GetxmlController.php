<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Vendor;
use App\Admin\Currency;
use App\Admin\Category;
use App\Admin\Product;
use App\Admin\Getxml;

class GetxmlController extends Controller
{
    protected $shop = array();
    protected $currencies = array();
    protected $categories = array();
    protected $eol = "\n";
	protected $yml = '';
	protected $offers;

	public function index()
	{
		return view('admin.getxml');
	}

    public function export(Request $request)
    {
        header('Content-Type: application/xml');
        header("Access-Control-Allow-Origin: *");
        $this->setShop('name', 'diceramica');
        $this->setShop('company', 'diceramica');
        $this->setShop('url', 'https://' . $_SERVER['HTTP_HOST'] . '/');
        $this->setShop('phone', '123456789');
        $this->setShop('platform', 'Yandex.YML for OpenCart (ocStore)');
        $this->setShop('version', '1.8.7');

        $currencies_db = Currency::all();

        $currencies = [];
		foreach($currencies_db as $currency){
            $currencies[$currency->code] = $currency;
		}

		foreach($currencies as $currency){
			$this->setCurrency($currency->code, $currency->rate);
		}
        
        $categories_db = Category::all();

        $categories = [];
		foreach($categories_db as $category){
            if($category->parent_id == 0){
            	unset($category->parent_id);
            }

            $categories[$category->id] = $category;
		}

		$this->categories = $categories;

		$yml  = '<?xml version="1.0" encoding="UTF-8"?>' . $this->eol;
		$yml .= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">' . $this->eol;
		$yml .= '<yml_catalog date="' . date('Y-m-d H:i') . '">' . $this->eol;
		$yml .= '<shop>' . $this->eol;
		$yml .= $this->array2Tag($this->shop);

		$yml .= '<currencies>' . $this->eol;

		foreach ($this->currencies as $currency) {
			$yml .= $this->getElement($currency, 'currency');
		}

		$yml .= '</currencies>' . $this->eol;

		$yml .= '<categories>' . $this->eol;

		foreach($this->categories as $category){
			$category = json_decode(json_encode($category, JSON_UNESCAPED_UNICODE), true);
			$category_name = $this->prepareField($category['name']);
			unset($category['name'], $category['created_at'], $category['updated_at']);
			$yml .= $this->getElement($category, 'category', $category_name);
		}
		$yml .= '</categories>' . $this->eol;
		
		$yml .= '<offers>' . $this->eol;

		$yml = $this->setProducts($yml, $request->start, $request->end);

		$yml .= '</offers>' . $this->eol
			.'</shop>'
			.'</yml_catalog>';

		file_put_contents('xml/xml-' . $request->start . '-' . $request->end . '.xml', $yml, LOCK_EX);

		echo $yml;
    }

    private function setProducts($yml, $start, $end)
    {

        //$products = Product::all();
    	$products = Product::getProducts($start, $end);
        
        $i = 1;
    	foreach($products as $product){

    		$product = json_decode(json_encode($product, JSON_UNESCAPED_UNICODE), true);

    		$data = [];

    		$data['id'] = $product['id'];
            $data['available'] = $product['available'];
            $data['url'] = $product['url'];
            $data['price'] = $product['price'];
            $data['oldprice'] = $product['oldprice'];
            $data['currencyId'] = 'UAH';
            $data['categoryId'] = $product['category'];
            $data['name'] = $product['name'];
            //$data['name_ua'] = $product['name_ua'];
            $data['vendor'] = $product['manufacturer'];
            $data['stock_quantity'] = $product['quantity'];

            $product['picture'] = json_decode($product['picture'], true);

            if ($product['picture']) {
				$data['picture'] = $product['picture'];
			}

			$data['description'] = '<![CDATA['.$product['description'].']]>';
			//$data['description_ua'] = '<![CDATA['.$product['description_ua'].']]>';

			$data['param'] = [];

			$attributes = json_decode($product['params'], true);
			$i = 0;

			foreach($attributes as $key => $value){
				$data['param'][$i]['name'] = $key;
				$data['param'][$i]['value'] = $value;
				$i++;
			}

			//$data['param_ua'] = [];

			// $attributes = json_decode($product['params_ua'], true);
			// $i = 0;

			// foreach($attributes as $key => $value){
			// 	$data['param_ua'][$i]['name'] = $key;
			// 	$data['param_ua'][$i]['value'] = $value;
			// 	$i++;
			// }

			$this->setOffer($data);
			$i++;
    	}

        foreach ($this->offers as $idx=>$offer) {
			$this->offers[$idx] = null;
			$tags = $this->array2Tag($offer['data']);
			unset($offer['data']);
			if (isset($offer['age'])) {
				$tags .= '<age unit="year">18</age>'.$this->eol;
				unset($offer['age']);
			}
			if (isset($offer['param'])) {
				$tags .= $this->array2Param($offer['param']);
				unset($offer['param']);
			}
			// if (isset($offer['param_ua'])) {
			// 	$tags .= $this->array2Paramua($offer['param_ua']);
			// 	unset($offer['param_ua']);
			// }
			$yml.= $this->getElement($offer, 'offer', $tags);
		}

		$this->offers = array();

		return $yml;
    }

    private function setOffer($data)
    {
    	$offer = array();

		$attributes = array('id', 'type', 'available', 'bid', 'cbid', 'param', 'delivery-option', 'group_id', 'accessory', 'age');
		$attributes = array_intersect_key($data, array_flip($attributes));

		foreach ($attributes as $key => $value) {
			switch ($key)
			{
				case 'id':
					$offer['id'] = $value;
					break;
				case 'bid':
				case 'cbid':
				case 'group_id':
					$value = (int)$value;
					if ($value > 0) {
						$offer[$key] = $value;
					}
					break;
					
				case 'type':
					if (in_array($value, array('vendor.model', 'book', 'audiobook', 'artist.title', 'tour', 'ticket', 'event-ticket'))) {
						$offer['type'] = $value;
					}
					break;

				case 'available':
					$offer['available'] = ($value ? 'true' : 'false');
					break;

				case 'param':
					if (is_array($value)) {
						$offer['param'] = $value;
					}
					break;

				// case 'param_ua':
				// 	if (is_array($value)) {
				// 		$offer['param_ua'] = $value;
				// 	}
				// 	break; 	

				case 'accessory':
					if (is_array($value)) {
						$offer['accessory'] = $value;
					}
					break;
					
				case 'delivery-option':
					if (is_array($value)) {
						$offer['delivery-option'] = $value;
					}
					break;

                case 'age':
                    $offer['age'] = $value;
					break;

				default:
					break;
			}
		}

		$type = isset($offer['type']) ? $offer['type'] : '';

		$allowed_tags = array('url'=>0, 'buyurl'=>0, 'price'=>1, 'oldprice'=>0, 'wprice'=>0, 'currencyId'=>1, 'xCategory'=>0, 'categoryId'=>1, 'picture'=>0, 'store'=>0, 'pickup'=>0, 'delivery'=>0, 'deliveryIncluded'=>0, 'local_delivery_cost'=>0, 'orderingTime'=>0, 'min-quantity'=>0, 'step-quantity'=>0, 'stock_quantity'=>0);

		switch ($type) {
			case 'vendor.model':
				$allowed_tags = array_merge($allowed_tags, array('typePrefix'=>0, 'vendor'=>1, 'vendorCode'=>0, 'model'=>1, 'provider'=>0, 'tarifplan'=>0));
				break;

			case 'artist.title':
				$allowed_tags = array_merge($allowed_tags, array('artist'=>0, 'title'=>1, 'year'=>0, 'media'=>0, 'starring'=>0, 'director'=>0, 'originalName'=>0, 'country'=>0));
				break;

			case 'tour':
				$allowed_tags = array_merge($allowed_tags, array('worldRegion'=>0, 'country'=>0, 'region'=>0, 'days'=>1, 'dataTour'=>0, 'name'=>1, 'hotel_stars'=>0, 'room'=>0, 'meal'=>0, 'included'=>1, 'transport'=>1, 'price_min'=>0, 'price_max'=>0, 'options'=>0));
				break;

			case 'event-ticket':
				$allowed_tags = array_merge($allowed_tags, array('name'=>1, 'place'=>1, 'hall'=>0, 'hall_part'=>0, 'date'=>1, 'is_premiere'=>0, 'is_kids'=>0));
				break;

			default:
				$allowed_tags = array_merge($allowed_tags, array('name'=>1, 'vendor'=>0, 'vendorCode'=>0));
				break;
		}

		$allowed_tags = array_merge($allowed_tags, array('aliases'=>0, 'additional'=>0, 'description'=>0, 'sales_notes'=>0, 'promo'=>0, 'manufacturer_warranty'=>0, 'country_of_origin'=>0, 'weight'=>0, 'downloadable'=>0, 'adult'=>0, /*'age'=>0,*/ 'barcode'=>0, 'keywords'=>0, 'rec'=>0));

		$required_tags = array_filter($allowed_tags);

		if (sizeof(array_intersect_key($data, $required_tags)) != sizeof($required_tags)) {
			return;
		}

		$data = array_intersect_key($data, $allowed_tags);

		$allowed_tags = array_intersect_key($allowed_tags, $data);

		// Стандарт XML учитывает порядок следования элементов,
		// поэтому важно соблюдать его в соответствии с порядком описанным в DTD
		$offer['data'] = array();
		foreach ($allowed_tags as $key => $value) {
			if (!isset($data[$key]))
				continue;
			if (is_array($data[$key])) {
				foreach ($data[$key] as $i => $val) {
					$offer['data'][$key][$i] = $this->prepareField($val);
				}
			}
			else {
				$offer['data'][$key] = $this->prepareField($data[$key]);
			}
		}

		$this->offers[] = $offer;
    }

    private function setShop($name, $value) {
		$allowed = array('name', 'company', 'url', 'phone', 'platform', 'version', 'agency', 'email');
		if (in_array($name, $allowed)) {
			$this->shop[$name] = $this->prepareField($value);
		}
	}

	private function getElement($attributes, $element_name, $element_value = '') {
		$retval = '<' . $element_name . ' ';
		foreach ($attributes as $key => $value) {
			$retval .= $key . '="' . $value . '" ';
		}
		$retval .= $element_value ? '>' . $this->eol . $element_value . '</' . $element_name . '>' : '/>';
		$retval .= $this->eol;

		return $retval;
	}

	private function setCurrency($id, $rate = 'CBRF', $plus = 0) {
		$allow_id = array('RUR', 'RUB', 'USD', 'BYR', 'BYN', 'KZT', 'EUR', 'UAH');
		if (!in_array($id, $allow_id)) {
			return false;
		}
		$allow_rate = array('CBRF', 'NBU', 'NBK', 'CB');
		if (in_array($rate, $allow_rate)) {
			$plus = str_replace(',', '.', $plus);
			if (is_numeric($plus) && $plus > 0) {
				$this->currencies[] = array(
					'id'=>$this->prepareField(strtoupper($id)),
					'rate'=>$rate,
					'plus'=>(float)$plus
				);
			} else {
				$this->currencies[] = array(
					'id'=>$this->prepareField(strtoupper($id)),
					'rate'=>$rate
				);
			}
		} else {
			$rate = str_replace(',', '.', $rate);
			if (!(is_numeric($rate) && $rate > 0)) {
				return false;
			}
			$this->currencies[] = array(
				'id'=>$this->prepareField(strtoupper($id)),
				'rate'=>(float)$rate
			);
		}

		return true;
	}

	private function array2Tag($tags) {
		$retval = '';
		foreach ($tags as $key => $value) {
			if (is_array($value)) {
				foreach ($value as $val) {
					$retval .= '<' . $key . '>' . $val . '</' . $key . '>' . $this->eol;
				}
			}
			else {
				$retval .= '<' . $key . '>' . $value . '</' . $key . '>' . $this->eol;
			}
		}

		return $retval;
	}

	private function array2Param($params) {
		$retval = '';
		foreach ($params as $param) {
			$retval .= '<param name="' . $this->prepareField($param['name']);
			if (isset($param['unit'])) {
				$retval .= '" unit="' . $this->prepareField($param['unit']);
			}
			$retval .= '">' . $this->prepareField($param['value']) . '</param>' . $this->eol;
		}

		return $retval;
	}

	private function array2Paramua($params) {
		$retval = '';
		foreach ($params as $param) {
			$retval .= '<param_ua name="' . $this->prepareField($param['name']);
			if (isset($param['unit'])) {
				$retval .= '" unit="' . $this->prepareField($param['unit']);
			}
			$retval .= '">' . $this->prepareField($param['value']) . '</param_ua>' . $this->eol;
		}

		return $retval;
	}

	private function prepareField($field) {
		if(is_array($field)){
			$field = '';
		}
		$field = htmlspecialchars_decode($field);
		//Убираем не UTF-8 символы
		//@todo использовать github.com/neitanod/forceutf8 для их конвертации
		$field = mb_convert_encoding($field, 'UTF-8', 'UTF-8');
		if (strpos($field, '<![CDATA[') === 0) {
			return trim($field);
		}
		$field = strip_tags($field);
		$from = array('&nbsp;', '&', '"', '>', '<', '\'');
		$to = array(' ', '&amp;', '&quot;', '&gt;', '&lt;', '&apos;');
		$field = str_replace($from, $to, $field);
		/**
		if ($this->from_charset != 'windows-1251') {
			$field = iconv($this->from_charset, 'windows-1251//IGNORE', $field);
		}
		**/
		$field = preg_replace('#[\x00-\x08\x0B-\x0C\x0E-\x1F]+#is', ' ', $field);

		return trim($field);
	}
}