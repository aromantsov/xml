<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Vendor;
use App\Admin\Category;
use App\Admin\Product;
use App\Admin\Currency;
use App\Translator;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function upload($vendor)
    {
        $vendor_data = Vendor::find($vendor);

        if($vendor_data->file_link && ($vendor_data->file_link != 'http://')){
            $xml = simplexml_load_file($vendor_data->file_link);
        }elseif($vendor_data->file){
            $xml = simplexml_load_file($vendor_data->file);
        }
        $path = $vendor_data->products;
        $path_array = explode('.', $path);
        $object_path = $xml;

        foreach($path_array as $tag){
            $object_path = $object_path->{$tag};
        }

        $object_path = (array)$object_path;

        $categories = json_decode($vendor_data->categories);
        
        $catpath = $vendor_data->categories_sector;
        $cat_array = explode('.', $catpath);
        $cat_path = $xml;
        foreach($cat_array as $tag){
            $cat_path = $cat_path->{$tag};
        }

        $file_categories = $cat_path->category;
        
        $cats = [];
        foreach($file_categories as $category){
            $cats[(string)$category['id']] = (string)$category;
        }

        foreach($object_path[$vendor_data->offer_tag] as $product){
            $data['vendor_id'] = $vendor;
            
            $data['src_id'] = $this->getProperty($product, $vendor_data->tag_id);
            
            $available = (bool)$this->getProperty($product, $vendor_data->tag_available);

            if($available){
                $data['available'] = 1;
            }else{
                $data['available'] = 0;
            }

            $data['group_id'] = $this->getProperty($product, $vendor_data->tag_group);
            
            $price = $this->getProperty($product, $vendor_data->tag_price);
            $oldprice = $this->getProperty($product, $vendor_data->tag_oldprice);

            $data['price'] = $this->setPrice($price, $vendor_data->changeprice, $this->getProperty($product, $vendor_data->tag_currency));
            $data['oldprice'] = $this->setPrice($oldprice, $vendor_data->changeprice, $this->getProperty($product, $vendor_data->tag_currency));

            $data['quantity'] = $this->getProperty($product, $vendor_data->tag_quantity);

            $pictures = (array)$this->getProperty($product, $vendor_data->tag_picture);
            $data['picture'] = json_encode($pictures, JSON_UNESCAPED_UNICODE);

            if($vendor_data->language == 1){

                $name = (string)$this->getProperty($product, $vendor_data->tag_name);

                $data['name'] = $name;
                
                // $tr_name = new Translator($name, 'ru', 'uk');
                // $data['name_ua'] = $tr_name->exec();

                $des = (string)$this->getProperty($product, $vendor_data->tag_description);

                $data['description'] = $des;
                
                // $tr_des = new Translator(strip_tags($des), 'ru', 'uk');
                // $data['description_ua'] = $tr_des->exec();

                $params = [];
                $params_ua = [];
                foreach($this->getProperty($product, $vendor_data->tag_param) as $param){
                    $params[(string)$param['name']] = (string)$param;
                    
                    // $tr_param_name_ua = new Translator($param['name'], 'ru', 'uk');
                    // $param['name_ua'] = $tr_param_name_ua->exec();
                    
                    // $tr_params_ua = new Translator((string)$param, 'ru', 'uk');
                    // $params_ua[(string)$param['name_ua']] = $tr_params_ua->exec();
                }              

                $data['params'] = json_encode($params, JSON_UNESCAPED_UNICODE);

                $data['params_ua'] = json_encode($params_ua, JSON_UNESCAPED_UNICODE);

            }elseif($vendor_data->language == 2){
               $name = (string)$this->getProperty($product, $vendor_data->tag_name);

                $data['name_ua'] = $name;

                // $tr_name = new Translator($name, 'uk', 'ru');
                // $data['name'] = $tr_name->exec();

                $des = (string)$this->getProperty($product, $vendor_data->tag_description);  

                $data['description_ua'] = $des;
                
                // $tr_des = new Translator(strip_tags($des), 'uk', 'ru');
                // $data['description_ua'] = $tr_des->exec();

                $params_ua = [];
                $params = [];
                foreach($this->getProperty($product, $vendor_data->tag_param) as $param){
                    $params_ua[(string)$param['name']] = (string)$param;

                    // $tr_param_name_ru = new Translator($param['name'], 'uk', 'ru');
                    // $param['name_ru'] = $tr_param_name_ua->exec();

                    // $tr_param = new Translator((string)$param, 'uk', 'ru');
                    // $params[(string)$param['name_ru']] = $tr_param->exec();
                }

                $data['params'] = json_encode($params, JSON_UNESCAPED_UNICODE);

                //$data['params_ua'] = json_encode($params_ua, JSON_UNESCAPED_UNICODE);
            }

            $url = $this->getProperty($product, $vendor_data->tag_url);

            $url_array = explode('/', $url);
            
            if($url_array[count($url_array) - 1]){
                $data['url'] = $vendor . '-' . $url_array[count($url_array) - 1];
            }else{
                $data['url'] = $vendor . '-' . $this->regTranslitIt($data['name']);
            }

            $data['manufacturer'] = (string)$this->getProperty($product, $vendor_data->tag_manufacturer);

            $data['barcode'] = (string)$this->getProperty($product, $vendor_data->tag_barcode);

            $category_id = $this->getProperty($product, $vendor_data->tag_category); 

            $final_category = [];

            foreach($categories as $cat_key => $cat_value){
                $values = explode(', ', $cat_value);

                foreach($values as $c_value){
                    if($category_id == $c_value){
                        $final_category[] = $cat_key;
                    }
                }
            }

            // if(!isset($final_category)){
            //     $final_category = $cats[(string)$category_id];
            // }
            // if(isset($final_category)){
            //     $data['category'] = $final_category;
            // }else{
            //     $data['category'] = '';
            // }

            $isset_product = Product::getProduct($data);
            
            if($final_category){
                $data['category'] = $final_category;
                if($isset_product){
                    Product::updateProduct($data);
                }else{
                    Product::addProduct($data);
                }
            }
            
            unset($final_category);
        }

        return redirect()->route('vendors');
    }

    private function getProperty($offer, $param)
    {
        $param_array = explode('@', $param);

        if((count($param_array) > 1) && (empty($param_array[0]))){
            return (string)$offer[$param_array[1]];
        }
        return $offer->{$param};
    }

    private function setPrice($price, $changeprice, $currency_code)
    {
        $currency = Currency::getCurrencyByCode($currency_code);

        if(isset($currency->rate)){
            $result = $price * $changeprice * $currency->rate;
        }else{
            $result = $price * $changeprice;
        }
        return $result;
    }

    private function translitIt($str)
    {
        $tr = array(
            "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g","Д"=>"d",
            "Е"=>"e","Ё"=>"yo","Ж"=>"j","З"=>"z","И"=>"i",
            "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
            "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
            "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"c","Ч"=>"ch",
            "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
            "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ё"=>"yo","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"c","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"y","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
            " "=> "_", "."=> "", "/"=> "_"
        );
        return strtr($str,$tr);
    }

    private function regTranslitIt($urlstr)
    {
        if (preg_match('/[^A-Za-z0-9_\-]/', $urlstr)) {
            $urlstr = $this->translitIt($urlstr);
            $urlstr = preg_replace('/[^A-Za-z0-9_\-]/', '', $urlstr);
            return $urlstr;
        }
    }

}
