<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Vendor;
use App\Admin\Category;
use App\Admin\Product;
use App\Admin\Currency;
use App\Translator;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $categories = Category::where('parent_id', '=', 0)
            ->with('children')
            ->get();
        
        return view('admin.categories', ['categories' => $categories]);    
    }

    public static function getParent($parent_id)
    {
        return Category::getParent($parent_id);
    }

    public function add()
    {
        $categories = Category::where('parent_id', '=', 0)
            ->with('children')
            ->get();
    	return view('admin.addcategory', ['categories' => $categories]);
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:categories',
        ]);

        Category::create([
            'name' => $request['name'],
            'parent_id' => $request['parent_id']
        ]);
        return redirect()->route('categories');
    }

    public function destroy($category)
    {
        Category::destroy($category);
        return redirect()->route('categories');
    }

    public function edit($category_id)
    {
        $category = Category::find($category_id);
        $all_categories = Category::where('parent_id', '=', 0)
            ->with('children')
            ->get();
        return view('admin.editcategory', ['category' => $category, 'all_categories' => $all_categories]);
    }

    public function update(Request $request, $category)
    {
        $this->validate($request, [
            'name' => ['required', 'string', \Illuminate\Validation\Rule::unique('categories')->ignore($category)],
        ]);

        Category::updateCategory($category, $request);
        return redirect()->route('categories');
    }
}