<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middware'=>['admin']], function(){
    Route::get('/', 'DashboardController@index')->name('admin'); 
    Route::get('/vendors', 'VendorController@index')->name('vendors'); 
    Route::get('/addvendor', 'VendorController@add')->name('addvendor');
    Route::post('/savevendor', 'VendorController@upload')->name('savevendor');
    Route::delete('/removevendor/{vendor}', 'VendorController@destroy')->name('removevendor');
    Route::get('/editvendor/{vendor}', 'VendorController@edit')->name('editvendor');
    Route::put('/editvendor/{vendor}', 'VendorController@update')->name('updatevendor');

    Route::get('/upload/{vendor}', 'ProductController@upload')->name('upload');

    Route::get('/currencies', 'CurrencyController@index')->name('currencies');
    Route::get('/addcurrency', 'CurrencyController@add')->name('addcurrency');
    Route::post('/savecurrency', 'CurrencyController@upload')->name('savecurrency');
    Route::delete('/deletecurrency/{currency}', 'CurrencyController@destroy')->name('deletecurrency');
    Route::get('/editcurrency/{currency}', 'CurrencyController@edit')->name('editcurrency');
    Route::put('/editcurrency/{currency}', 'CurrencyController@update')->name('updatecurrency');

    Route::get('/categories', 'CategoryController@index')->name('categories');
    Route::get('/addcategory', 'CategoryController@add')->name('addcategory');
    Route::post('/savecategory', 'CategoryController@upload')->name('savecategory');
    Route::delete('/deletecategory/{category}', 'CategoryController@destroy')->name('deletecategory');
    Route::get('/editcategory/{category}', 'CategoryController@edit')->name('editcategory');
    Route::put('/editcategory/{category}', 'CategoryController@update')->name('updatecategory');

    Route::get('/getxml', 'GetxmlController@index')->name('xml');
    Route::get('/export', 'GetxmlController@export')->name('export');
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

